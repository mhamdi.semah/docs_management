package com.octadev.docs_management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.octadev.docs_management.domain.Api;
import com.octadev.docs_management.domain.Headers;
import com.octadev.docs_management.repository.ApiRepository;
import com.octadev.docs_management.repository.HeadersRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class Test {

	@Autowired
	ApiRepository apiRepository;
	@Autowired
	HeadersRepository headersRepository;
	List<Headers> l ;
	@GetMapping("/test")
	public void add()
	{
		l = new ArrayList<>();
		Api a =new Api();
		a.setEndpoint("google.com");
		Headers h = new Headers();
		h.setApi(a);
		h.setCle("test");
		l.add(h);
		a.setHeaders(l);
		apiRepository.save(a);
	}
	@GetMapping("/get")
	public void get()
	{
		System.out.println("entred");
		Optional<Api> a = apiRepository.findById(1L);
		if(a.isPresent())
		{
			Api a1 =a.get();
			System.out.println(a1.getEndpoint());
		}
		else
			System.out.println("false");
	}
}
