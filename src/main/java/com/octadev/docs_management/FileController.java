package com.octadev.docs_management;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.activation.FileTypeMap;
import javax.imageio.ImageIO;
import javax.websocket.server.PathParam;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileController {
 @Autowired
 FileService fileservice;
 @CrossOrigin(origins = "http://localhost:4200") // Call  from Local Angualar
 @PostMapping("/profile/uploadpicture")
 public ResponseEntity < String > handleFileUpload(@RequestParam("file") MultipartFile file) {
  String message = "";
  try {
   fileservice.store(file);
   message = "You successfully uploaded " + file.getOriginalFilename() + "!";
   return ResponseEntity.status(HttpStatus.OK).body(message);
  } catch (Exception e) {
   message = "Fail to upload Profile Picture" + file.getOriginalFilename() + "!";
   return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
  }
 }
 
 @GetMapping(path="showme/{image}",produces= {MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_PNG_VALUE})
 public ResponseEntity<byte[]> getImage(@PathVariable(value = "image") String image) throws IOException{
     System.out.println("ena hna "+image);
	 File img = new File("ProfilePictureStore/"+image);
     return ResponseEntity.ok().body(Files.readAllBytes(img.toPath()));
 }
}