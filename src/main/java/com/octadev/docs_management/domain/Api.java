package com.octadev.docs_management.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Api {
	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("endpoint")
	private String endpoint;
	@JsonProperty("desc")
	private String desciption;
	
	@ManyToOne
	@JoinColumn(name="partie_id")
	private Partie partie;
	
	@OneToMany(mappedBy="api",cascade= CascadeType.ALL)
	private List<Headers> headers;
	
	@OneToMany(mappedBy="api")
	private List<Params> params;

}
