package com.octadev.docs_management.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Projet {
	
	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("titre")
	private String titre;
	@JsonProperty("desc")
	private String desciption;
	@JsonProperty("version")
	private String version;
	@JsonProperty("etat")
	private String etat;
	
	@OneToOne
    @JoinColumn(name = "manual_id")
	@JsonIgnore
	private Manuel_util manuel_util;
	
	@OneToMany(mappedBy="projet")
	@JsonIgnore
	private List<Conception> conceptions;
	
	@OneToMany(mappedBy="projet")
	@JsonIgnore
	private List<Partie> parties;

}
