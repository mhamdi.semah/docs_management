package com.octadev.docs_management.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Conception {

	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("desc")
	private String desciption;
	@JsonProperty("modele")
	private String modele;
	
	@ManyToOne
    @JoinColumn(name="projet_id")
	private Projet projet;
}
