package com.octadev.docs_management.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Code {

	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("titre")
	private String titre;
	@JsonProperty("desc")
	private String desciption;
	@JsonProperty("code")
	private String code_source;
	@JsonProperty("package")
	private String pack;
	
	@ManyToOne
	@JoinColumn(name="partie_id")
	private Partie partie;
}
