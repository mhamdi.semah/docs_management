package com.octadev.docs_management.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Partie {
	
	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("desc")
	private String desciption;
	@JsonProperty("statut")
	private String statut;
	
	@ManyToOne
	@JoinColumn(name="projet_id")
	private Projet projet;
	
	@OneToMany(mappedBy="partie")
	@JsonIgnore
	private List<Api> apis;
	
	@OneToMany(mappedBy="partie")
	private List<Composant> composants;
	
	@OneToMany(mappedBy="partie")
	@JsonIgnore
	private List<Code> codes;
	

}
