package com.octadev.docs_management.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Sujet {

	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("titre")
	private String titre;
	@JsonProperty("desc")
	private String desciption;
	
	@ManyToOne
    @JoinColumn(name="manuel_id")
	private Manuel_util manuel_util;
	
	@OneToMany(mappedBy="sujet")
	private List<Etape> etapes;
}
