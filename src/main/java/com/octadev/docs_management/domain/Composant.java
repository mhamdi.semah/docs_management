package com.octadev.docs_management.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Composant {
	
	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("tech")
	private String technologie;
	@JsonProperty("desc")
	private String desciption;
	@JsonProperty("version")
	private String version;
	
	@ManyToOne
	@JoinColumn(name="partie_id")
	private Partie partie;
	
}
