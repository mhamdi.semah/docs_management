package com.octadev.docs_management.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Etape {

	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("titre")
	private String titre;
	@JsonProperty("desc")
	@Lob
	@Column( columnDefinition="TEXT")
	private String description;
	@JsonProperty("img")
	private String screenshot;
	
	@ManyToOne
    @JoinColumn(name="sujet_id")
	private Sujet sujet;
	
}
