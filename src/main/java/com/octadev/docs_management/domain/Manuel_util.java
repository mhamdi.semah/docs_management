package com.octadev.docs_management.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Manuel_util {

	@Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
    private long id;
	@JsonProperty("titre")
	private String titre;
	@JsonProperty("desc")
	private String desciption;
	
	@OneToOne(mappedBy = "manuel_util")
	private Projet projet;
	
	@OneToMany(mappedBy = "manuel_util")
	private List<Sujet> sujets;
}
