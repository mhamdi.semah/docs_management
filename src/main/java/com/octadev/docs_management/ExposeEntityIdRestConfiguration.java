package com.octadev.docs_management;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.octadev.docs_management.domain.Etape;
import com.octadev.docs_management.domain.Manuel_util;
import com.octadev.docs_management.domain.Projet;
import com.octadev.docs_management.domain.Sujet;

@Configuration
public class ExposeEntityIdRestConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Projet.class);
        config.exposeIdsFor(Manuel_util.class);
        config.exposeIdsFor(Sujet.class);
        config.exposeIdsFor(Etape.class);
    }
}
