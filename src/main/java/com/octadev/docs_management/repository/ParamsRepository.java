package com.octadev.docs_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.octadev.docs_management.domain.Params;

@RepositoryRestResource(collectionResourceRel = "params", path = "params")
@CrossOrigin
public interface ParamsRepository extends JpaRepository<Params, Long>{

}
