package com.octadev.docs_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.octadev.docs_management.domain.Manuel_util;

@RepositoryRestResource(collectionResourceRel = "manuel", path = "manuel_util")
@CrossOrigin
public interface Manuel_utilRepository extends JpaRepository<Manuel_util, Long>{

}
