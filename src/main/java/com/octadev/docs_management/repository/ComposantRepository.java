package com.octadev.docs_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.octadev.docs_management.domain.Composant;

@RepositoryRestResource(collectionResourceRel = "composant", path = "composant")
@CrossOrigin
public interface ComposantRepository extends JpaRepository<Composant, Long>{

}
