package com.octadev.docs_management.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.octadev.docs_management.domain.Api;

@RepositoryRestResource(collectionResourceRel = "api", path = "api")
@CrossOrigin
public interface ApiRepository extends JpaRepository<Api, Long>{
	List<Api> findAllByEndpoint(@Param("endpoint") String endpoint);

}
